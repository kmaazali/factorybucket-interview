import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
export const useBookStore = defineStore({
  id: "books",
  state: () => ({
    booksData: []
  }),
  getters: {
    getBooks: (state) => { return state.booksData }
  },
  actions: {
    addBook(title,publishYear,numberOfPages){
      this.booksData.push({
        'bookTitle':title,
        'yearPublished':publishYear,
        'numberOfPages':numberOfPages
      })
    },
    clearBooks(){
      this.booksData=[]
    }
  },
});