import { createRouter, createWebHistory } from 'vue-router'
import Home from '../components/Home.vue'
import ListBook from '../components/ListBook.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/listbook',
      name: 'listbook',
      component: ListBook
    }
  ]
})

export default router
